package rs.hybrit.library.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class BookCopy extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    private Book book;

    @Column
    private LocalDate rentDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    public BookCopy() { }

    public BookCopy(Book book) {
        this.book = book;
    }

    public BookCopy(Book book, LocalDate rentDate) {
        this.book = book;
        this.rentDate = rentDate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LocalDate getRentDate() {
        return rentDate;
    }

    public void setRentDate(LocalDate rentDate) {
        this.rentDate = rentDate;
    }

    public User getUser() { return user; }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isRented() {
        return rentDate != null;
    }

    public boolean isAvailable() {
        return rentDate == null;
    }

    public void doRent() {
        this.rentDate = LocalDate.now();
    }

    public void doReturn() {
        this.rentDate = null;
    }

    public String toString() {
        return "Book copy { id: " +
                ", rent date: " + rentDate +
                ", book: " + book + "}";
    }
}
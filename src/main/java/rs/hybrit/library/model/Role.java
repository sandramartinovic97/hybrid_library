package rs.hybrit.library.model;

public enum Role {
    ROLE_REGULAR, ROLE_ADMIN
}

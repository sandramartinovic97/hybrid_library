package rs.hybrit.library.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class User extends AbstractEntity {

    @Column
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column
    @NotNull(message = "User's username can not be empty.")
    @Size(max = 255, message = "Size of the user's username must be maximum 255.")
    private String username;

    @Column
    @NotNull(message = "User's email can not be empty.")
    @Size(max = 255, message = "Size of the user's email must be maximum 255.")
    private String email;

    @Column
    @NotNull(message = "User's first name can not be empty.")
    @Size(max = 255, message = "Size of the user's first name must be maximum 255.")
    private String firstName;

    @Column
    @NotNull(message = "User's last name can not be empty.")
    @Size(max = 255, message = "Size of the user's last name must be maximum 255.")
    private String lastName;

    @Column
    @Size(min = 8, max = 255, message = "Size of the user's password must be between 8 and 255 characters.")
    private String password;

    public User() {

    }

    public static class Builder {
        private Long id;
        private String username;
        private String email;
        private String firstName;
        private String lastName;
        private String password;
        private Role role;

        public Builder(Long id) {
            this.id = id;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder role(Role role) {
            this.role = role;
            return this;
        }

        public User build() {
            User user = new User();
            user.username = this.username;
            user.email = this.email;
            user.firstName = this.firstName;
            user.lastName = this.lastName;
            user.password = this.password;
            user.role = this.role;
            return user;
        }
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "User { id: " +
                ", role: " + role +
                ", username: " + username +
                ", email: " + email +
                ", first name: " + firstName +
                ", last name: " + lastName +
                ", password: " + password + " }";
    }
}

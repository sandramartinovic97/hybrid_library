package rs.hybrit.library.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Book extends AbstractEntity {

    @Column
    @NotNull(message = "Name of the book can not be empty.")
    @Size(max=255, message = "Size of a name must be maximum 255.")
    private String name;

    @Column
    @NotNull(message = "Author's name can not be empty.")
    @Size(max=255, message = "Size of an author's name must be maximum 255.")
    private String author;

    @Column
    @NotNull(message = "ISBN of book can not be empty.")
    @Size(max=13, message = "Size of a ISBN must be maximum 13.")
    private String isbn;

    @OneToMany
    private List<BookCopy> bookCopies;

    public Book() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(List<BookCopy> bookCopies) {
        this.bookCopies = bookCopies;
    }

    public String toString() {
        return "Book { id: " +
        ", name: " + name +
        ", author: " + author +
        ", isbn: " + isbn + " }";
    }
}

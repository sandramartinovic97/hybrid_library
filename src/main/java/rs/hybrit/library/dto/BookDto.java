package rs.hybrit.library.dto;

public class BookDto {

    private Long id;

    private String name;

    private String author;

    private String isbn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public static class Builder {

        private Long id;
        private String name;
        private String author;
        private String isbn;

        public Builder() {

        }

        public Builder(Long id) {
            this.id = id;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder author(String author) {
            this.author = author;

            return this;
        }

        public Builder isbn(String isbn) {
            this.isbn = isbn;

            return this;
        }

        public BookDto build() {
            BookDto bookDto = new BookDto();
            bookDto.id = this.id;
            bookDto.name = this.name;
            bookDto.author = this.author;
            bookDto.isbn = this.isbn;

            return bookDto;
        }
    }

    private BookDto() {

    }
}

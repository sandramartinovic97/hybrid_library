package rs.hybrit.library.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class BookCopyDto {

    private Long id;

    private BookDto book;

    @JsonFormat(pattern = "yyyy-dd-MM")
    private LocalDate rentDate;

    private UserDto user;

    public BookCopyDto() { }

    public BookCopyDto(Long id, BookDto book, LocalDate rentDate, UserDto user) {
        this.id = id;
        this.book = book;
        this.rentDate = rentDate;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public LocalDate getRentDate() {
        return rentDate;
    }

    public void setRentDate(LocalDate rentDate) {
        this.rentDate = rentDate;
    }

    public UserDto getUser() { return user; }

    public void setUser(UserDto user) {
        this.user = user;
    }
}

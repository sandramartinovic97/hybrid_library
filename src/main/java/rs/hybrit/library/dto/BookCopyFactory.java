package rs.hybrit.library.dto;

import org.springframework.stereotype.Component;
import rs.hybrit.library.model.Book;
import rs.hybrit.library.model.BookCopy;

@Component
public class BookCopyFactory {

    private final UserFactory userFactory;
    private final BookFactory bookFactory;

    public BookCopyFactory(UserFactory userFactory, BookFactory bookFactory) {
        this.userFactory = userFactory;
        this.bookFactory = bookFactory;
    }

    public BookCopy toBookCopy(BookCopyDto bookCopyDto) {
        BookCopy bookCopy = new BookCopy();
        bookCopy.setId(bookCopyDto.getId());
        bookCopy.setRentDate(bookCopyDto.getRentDate());

        Book book = bookFactory.toBook(bookCopyDto.getBook());
        bookCopy.setBook(book);

        bookCopy.setUser(userFactory.toUser(bookCopyDto.getUser()));

        return bookCopy;
    }

    public BookCopyDto toBookCopyDto(BookCopy bookCopy) {
        var bookCopyDto = new BookCopyDto();
        bookCopyDto.setId(bookCopy.getId());

        BookDto book = bookFactory.toBookDto(bookCopy.getBook());
        bookCopyDto.setBook(book);

        bookCopyDto.setRentDate(bookCopy.getRentDate());

        if(bookCopy.getUser() != null) {
            bookCopyDto.setUser(userFactory.toUserDto(bookCopy.getUser()));
        }

        return bookCopyDto;
    }
}

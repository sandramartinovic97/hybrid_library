package rs.hybrit.library.dto;

import org.springframework.stereotype.Component;
import rs.hybrit.library.model.Book;

@Component
public class BookFactory {

    public Book toBook(BookDto bookDto) {
        Book book = new Book();
        book.setId(bookDto.getId());
        book.setName(bookDto.getName());
        book.setAuthor(bookDto.getAuthor());
        book.setIsbn(bookDto.getIsbn());

        return book;
    }

    public BookDto toBookDto(Book book) {
        return new BookDto.Builder(book.getId())
                .name(book.getName())
                .author(book.getAuthor())
                .isbn(book.getIsbn())
                .build();
    }

    public Book updateBook(Book book, BookDto bookDto) {
        book.setName(bookDto.getName());
        book.setAuthor(bookDto.getAuthor());
        book.setIsbn(bookDto.getIsbn());

        return book;
    }
}
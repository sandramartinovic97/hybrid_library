package rs.hybrit.library.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_REGULAR = "REGULAR";

    public SecurityConfig (UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/user/**", "/book/**", "/bookCopy/**")
                .hasAnyRole(ROLE_ADMIN, ROLE_REGULAR)

                .antMatchers(HttpMethod.PUT, "/book/**")
                .hasAnyRole(ROLE_ADMIN)

                .antMatchers(HttpMethod.POST, "/book/**")
                .hasAnyRole(ROLE_ADMIN)

                .antMatchers(HttpMethod.POST, "/book/rent/**", "/book/return/**")
                .hasAnyRole(ROLE_ADMIN, ROLE_REGULAR)

                .antMatchers(HttpMethod.DELETE, "/book/**")
                .hasAnyRole(ROLE_ADMIN)

                .antMatchers("/swagger-ui.html", "/h2-console/**").permitAll()
                .and()
                .csrf().disable()
                .headers().frameOptions().sameOrigin();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

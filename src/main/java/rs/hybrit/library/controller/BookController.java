package rs.hybrit.library.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.hybrit.library.dto.BookDto;
import rs.hybrit.library.service.BookService;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    private final BookService bookService;

    private static Logger logger = LoggerFactory.getLogger(BookController.class);

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<List<BookDto>> getBooks() {
        List<BookDto> bookDto = bookService.getBooks();
        logger.info("Getting all books...");

        return ResponseEntity.ok(bookDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> findBook(Long id) {
        BookDto bookDto = bookService.findBook(id);
        logger.info("Getting book with id = {}...", id);

        return ResponseEntity.ok(bookDto);
    }

    @PostMapping
    public ResponseEntity<BookDto> create(BookDto bookDto) {
        BookDto newBookDto = bookService.create(bookDto);
        logger.info("Creating book...");

        return ResponseEntity.ok(newBookDto);
    }

    @PutMapping
    ResponseEntity<BookDto> update(BookDto bookDto) {
        BookDto newBookDto = bookService.update(bookDto);
        logger.info("Updating book...");

        return ResponseEntity.ok(newBookDto);
    }

    @DeleteMapping("/{id}")
    public void delete(Long id){
        logger.info("Deleting book...");

        bookService.delete(id);
    }

    @PostMapping("/rent/{id}")
    public void rentBook(@PathVariable Long id) {
        bookService.rentBook(id);
        logger.info("Renting book with id = {}...", id);
    }

    @PostMapping("/return/{id}")
    public void returnBook(Long id) {
        bookService.returnBook(id);
        logger.info("Returning book with id = {}...", id);
    }
}

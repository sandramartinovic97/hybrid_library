package rs.hybrit.library.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.hybrit.library.dto.BookCopyDto;
import rs.hybrit.library.service.BookCopyService;
import java.util.List;

@RestController
@RequestMapping("/bookCopy")
public class BookCopyController {

    private final BookCopyService bookCopyService;

    private final static Logger logger = LoggerFactory.getLogger(BookCopyController.class);

    public BookCopyController(BookCopyService bookCopyService) {
        this.bookCopyService = bookCopyService;
    }

    @GetMapping
    public ResponseEntity<List<BookCopyDto>> getBookCopies() {
        List<BookCopyDto> bookCopyDto = bookCopyService.getBookCopies();
        logger.info("Getting all book copies...");

        return ResponseEntity.ok(bookCopyDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookCopyDto> findBookCopy(Long id) {
        BookCopyDto bookCopyDto = bookCopyService.findBookCopy(id);
        logger.info("Getting book copy with id = {}...", id);

        return ResponseEntity.ok(bookCopyDto);
    }

    @PostMapping
    public ResponseEntity<BookCopyDto> create(@RequestBody BookCopyDto bookCopyDto) {
        BookCopyDto newBookCopyDto = bookCopyService.create(bookCopyDto);
        logger.info("Creating book copy...");

        return ResponseEntity.ok(newBookCopyDto);
    }

    @DeleteMapping("/{id}")
    public void delete(Long id) {
        logger.info("Deleting book copy...");

        bookCopyService.delete(id);
    }
}
package rs.hybrit.library.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import rs.hybrit.library.dto.UserDto;
import rs.hybrit.library.dto.UserFactory;
import rs.hybrit.library.model.User;
import rs.hybrit.library.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserFactory userFactory;
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    public UserController(UserService userService, UserFactory userFactory) {
        this.userService = userService;
        this.userFactory = userFactory;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getUsers() {
        List<UserDto> userDto = userService.getUsers();

        logger.info("Getting all users...");

        return ResponseEntity.ok(userDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findUser(Long id) {
        UserDto userDto = userService.findUser(id);

        logger.info("Getting user with id = {}...", id);

        return ResponseEntity.ok(userDto);
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> findByUsername(String username) {
        User user = userService.findByUsername(username);

        logger.info("Getting user with username {}...", username);

        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}")
    public void delete(Long id) {
        logger.info("Deleting user...");

        userService.delete(id);
    }

    @GetMapping(value = "/login")
    private ResponseEntity login(@Valid UserDto userDto) {
        User user = userFactory.toUser(userDto);
        User userDB = userService.findByUsername(user.getUsername());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (bCryptPasswordEncoder.matches(user.getPassword(), userDB.getPassword())) {
            return ResponseEntity.ok(userDB);
        }
        else {
            return ResponseEntity.badRequest().body("Invalid password!");
        }
    }
}

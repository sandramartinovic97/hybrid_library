package rs.hybrit.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.hybrit.library.model.Book;
import rs.hybrit.library.model.BookCopy;

import java.util.List;

public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {

    List<BookCopy> findByBook(Book book);
}

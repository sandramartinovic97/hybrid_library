package rs.hybrit.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.hybrit.library.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}

package rs.hybrit.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.hybrit.library.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}

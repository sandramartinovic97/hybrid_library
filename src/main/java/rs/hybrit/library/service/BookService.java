package rs.hybrit.library.service;

import rs.hybrit.library.dto.BookDto;
import rs.hybrit.library.model.User;

import java.util.List;

public interface BookService {
    List<BookDto> getBooks();
    BookDto findBook(Long id);
    BookDto create(BookDto bookDto);
    BookDto update(BookDto bookDto);
    void delete(Long id);
    void rentBook(Long id);
    void returnBook(Long id);
}

package rs.hybrit.library.service;

import org.springframework.stereotype.Service;
import rs.hybrit.library.dto.BookCopyDto;
import rs.hybrit.library.dto.BookCopyFactory;
import rs.hybrit.library.model.Book;
import rs.hybrit.library.model.BookCopy;
import rs.hybrit.library.repository.BookCopyRepository;
import rs.hybrit.library.repository.BookRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookCopyServiceImpl implements BookCopyService {

    private final BookCopyFactory bookCopyFactory;
    private final BookCopyRepository bookCopyRepository;
    private final BookRepository bookRepository;

    public BookCopyServiceImpl(BookCopyFactory bookCopyFactory, BookCopyRepository bookCopyRepository, BookRepository bookRepository) {
        this.bookCopyFactory = bookCopyFactory;
        this.bookCopyRepository = bookCopyRepository;
        this.bookRepository = bookRepository;
    }

    public List<BookCopyDto> getBookCopies() {
        List<BookCopy> bookCopies = bookCopyRepository.findAll();

        List<BookCopyDto> result = new ArrayList<>();

        for (BookCopy bookCopy: bookCopies) {
            result.add(bookCopyFactory.toBookCopyDto(bookCopy));
        }

        return result;
    }

    public BookCopyDto findBookCopy(Long id) {
        BookCopy bookCopy = bookCopyRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find book copy with id = " + id + "."));

        return bookCopyFactory.toBookCopyDto(bookCopy);
    }

    public BookCopyDto create(BookCopyDto bookCopyDto) {
        Optional<Book> book = bookRepository.findById(bookCopyDto.getBook().getId());

        if(book.isPresent()) {
            BookCopy bookCopy = bookCopyFactory.toBookCopy(bookCopyDto);
            BookCopy newBookCopy = bookCopyRepository.save(bookCopy);
            return bookCopyFactory.toBookCopyDto(newBookCopy);
        } else {
            throw new EntityNotFoundException("Could not create new book copy because book with id = " + bookCopyDto.getBook().getId() + " is not found.");
        }
    }

    public void delete(Long id) {
        BookCopy bookCopy = bookCopyRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book copy with id = " + id + " is not found."));

        bookCopyRepository.delete(bookCopy);
    }

    public List<BookCopy> findByBook(Book book) {
        return bookCopyRepository.findByBook(book);
    }

    public void save(BookCopy bookCopy) {
        bookCopyRepository.save(bookCopy);
    }
}

package rs.hybrit.library.service;

import rs.hybrit.library.dto.BookCopyDto;
import rs.hybrit.library.model.Book;
import rs.hybrit.library.model.BookCopy;

import java.util.List;

public interface BookCopyService {
    List<BookCopyDto> getBookCopies();
    BookCopyDto findBookCopy(Long id);
    BookCopyDto create(BookCopyDto bookCopyDto);
    void delete(Long id);
    List<BookCopy> findByBook(Book book);
    void save(BookCopy bookCopy);
}

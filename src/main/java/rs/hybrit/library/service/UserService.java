package rs.hybrit.library.service;

import rs.hybrit.library.dto.UserDto;
import rs.hybrit.library.model.User;

import java.util.List;

public interface UserService {
    List<UserDto> getUsers();
    UserDto findUser(Long id);
    User findByUsername(String username);
    void delete(Long id);
}

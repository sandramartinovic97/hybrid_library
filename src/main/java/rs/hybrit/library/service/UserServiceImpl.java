package rs.hybrit.library.service;

import org.springframework.stereotype.Service;
import rs.hybrit.library.dto.UserDto;
import rs.hybrit.library.dto.UserFactory;
import rs.hybrit.library.model.User;
import rs.hybrit.library.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserFactory userFactory;

    public UserServiceImpl(UserRepository userRepository, UserFactory userFactory) {
        this.userRepository = userRepository;
        this.userFactory = userFactory;
    }

    public List<UserDto> getUsers() {
        List<User> users = userRepository.findAll();

        List<UserDto> result = new ArrayList<>();

        for(User user : users) {
            result.add(userFactory.toUserDto(user));
        }

        return result;
    }

    public UserDto findUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find user with id = " + id + "."));

        return userFactory.toUserDto(user);
    }

    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username);

        if(user == null) {
            throw new IllegalStateException("Username " + username + " does not exist.");
        }

        return user;
    }

    public void delete(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("User with id = " + id + " is not found."));
        userRepository.deleteById(id);
    }
}

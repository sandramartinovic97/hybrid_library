package rs.hybrit.library.service;

import org.springframework.stereotype.Service;
import rs.hybrit.library.dto.BookDto;
import rs.hybrit.library.dto.BookFactory;
import rs.hybrit.library.model.Book;
import rs.hybrit.library.model.BookCopy;
import rs.hybrit.library.repository.BookRepository;
import rs.hybrit.library.util.SecurityUtil;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class BookServiceImpl implements BookService {

    private final BookCopyService bookCopyService;
    private final BookFactory bookFactory;
    private final BookRepository bookRepository;
    private final UserService userService;

    public BookServiceImpl(BookCopyService bookCopyService, BookFactory bookFactory, BookRepository bookRepository, UserService userService) {
        this.bookCopyService = bookCopyService;
        this.bookFactory = bookFactory;
        this.bookRepository = bookRepository;
        this.userService = userService;
    }

    public List<BookDto> getBooks() {
        List<Book> books = bookRepository.findAll();

        List<BookDto> result = new ArrayList<>();

        for (Book book: books) {
            result.add(bookFactory.toBookDto(book));
        }

        return result;
    }

    public BookDto findBook(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find book with id = " + id + "."));

        return bookFactory.toBookDto(book);
    }

    public BookDto create(BookDto bookDto) {
        Book book = bookFactory.toBook(bookDto);
        Book newBook = bookRepository.save(book);

        return bookFactory.toBookDto(newBook);
    }

    public BookDto update(BookDto bookDto) {
        Book book = bookRepository.findById(bookDto.getId()).orElseThrow(() -> new EntityNotFoundException("Could not update book because id = " + bookDto.getId() + " is not found."));
        Book updatedBook = bookFactory.updateBook(book, bookDto);
        updatedBook = bookRepository.save(updatedBook);

        return bookFactory.toBookDto(updatedBook);
    }

    public void delete(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book with id = " + id + " is not found."));

        bookRepository.delete(book);
    }

    public void rentBook(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not rent book with id = " + id + " because it is not found."));

        boolean rented = false;
        List<BookCopy> bookCopies = bookCopyService.findByBook(book);

        for (BookCopy bookCopy: bookCopies) {
            if(bookCopy.isAvailable()) {
                bookCopy.doRent();
                bookCopy.setUser(userService.findByUsername(SecurityUtil.getLoggedInUsername()));
                bookCopyService.save(bookCopy);
                rented = true;
            }
        }

        if(!rented) {
            throw new EntityNotFoundException("No book copies found for renting.");
        }
    }

    public void returnBook(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not return book because book with id = " + id + " does not exist."));

        boolean returned = false;
        List<BookCopy> bookCopies = bookCopyService.findByBook(book);

        for (BookCopy bookCopy: bookCopies) {
            if(bookCopy.isRented()) {
                bookCopy.doReturn();
                bookCopy.setUser(null);
                bookCopyService.save(bookCopy);
                returned = true;
            }
        }

        if(!returned) {
            throw new EntityNotFoundException("No rented book copies found.");
        }
    }
}

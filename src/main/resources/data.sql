INSERT INTO book (name, author, isbn) VALUES ('The Great Gatsby', 'F. Scott Fitzgerald', '889562'),
('War and Peace', 'Leo Tolstoy', '1586952M'),
('Pride and Prejudice', 'Jane Austen', '78152KK'),
('The Hobbit', 'J. R. R. Tolkien', '95625562'),
('Harry Potter', 'J. K. Rowling', '4M524J6');

INSERT INTO book_copy (book_id, rent_date, user_id) VALUES (4, null, null),
(2, null, null),
(3, null, null),
(3, null, null),
(5, null, null);

INSERT INTO user (role, username, email, first_name, last_name, password)
VALUES ('ROLE_ADMIN', 'sandra', 'sandra@gmail.com', 'Sandra', 'Martinovic', '$2y$12$ra1qCNs87aVhvKgNFBmvQeS7MmN9x5NlvZSx.xvhvKUGqiWgOCfcq'),
('ROLE_REGULAR', 'tamara', 'tamara@gmail.com', 'Tamara', 'Jovicic', '$2y$12$bkVOjVVmkkuFehKGVLZjp.BCMZSMMnUVlBdYSE.Ode3wxK/oTSt3G'),
('ROLE_REGULAR', 'mirko', 'mirko@gmail.com', 'Mirko', 'Hajduk', '$2y$12$buVm3M1VDF5fMVBQro/vcOakilAVkfqgnq61nKa2YHhmr.3i3OXeG'),
('ROLE_REGULAR', 'marko', 'marko@gmail.com', 'Marko', 'Uzelac', '$2y$12$4qdAHoiMZDiKRHtJvzZV4ezrOZMWo5imDDFvVW7cKUPLZBIb7GZCW');
package rs.hybrit.library.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import org.mockito.junit.MockitoJUnitRunner;
import rs.hybrit.library.dto.BookDto;
import rs.hybrit.library.service.BookServiceImpl;

import java.util.ArrayList;
import java.util.Collections;

@RunWith(MockitoJUnitRunner.class)
public class BookControllerTest {

    @InjectMocks
    private BookController bookController;

    @Mock
    private BookServiceImpl bookServiceImpl;

    @Test
    public void getBooks_noBooksInDb_emptyListReturned() {
        when(bookServiceImpl.getBooks()).thenReturn(Collections.EMPTY_LIST);

        var result = bookController.getBooks().getBody();

        verify(bookServiceImpl).getBooks();
        assertThat(result).isEmpty();
    }

    @Test
    public void getBooks_oneBookInDb_listWithOneBookReturned() {
        var books = new ArrayList<BookDto>();
        var book = new BookDto.Builder(1L)
                .name("Harry Potter")
                .author("J. K. Rowling")
                .isbn("4M524J6")
                .build();
        books.add(book);

        when(bookServiceImpl.getBooks()).thenReturn(books);

        var result = bookController.getBooks().getBody();

        verify(bookServiceImpl).getBooks();
        assertThat(result).hasSize(1);
        assertThat(result).contains(book);
    }

    @Test
    public void findBook_idFromNonExistingBook_illegalStateExceptionThrown() {
        var id = 1L;

        when(bookServiceImpl.findBook(eq(id))).thenThrow(new IllegalStateException());

        Assertions.assertThatThrownBy(() -> bookController.findBook(id)).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void findBook_idFromExistingBook_bookIsReturned() {
        var id = 1L;
        var bookDto = new BookDto.Builder().build();

        when(bookServiceImpl.findBook(eq(id))).thenReturn(bookDto);

        var result = bookController.findBook(id).getBody();

        verify(bookServiceImpl).findBook(id);
        assertThat(result).isEqualTo(bookDto);
    }

    @Test
    public void create_bookDto_bookDtoIsCreated () {
        BookDto bookDto = new BookDto.Builder().build();

        when(bookServiceImpl.create(eq(bookDto))).thenReturn(bookDto);

        var result = bookController.create(bookDto).getBody();

        verify(bookServiceImpl).create(bookDto);
        assertThat(result).isEqualTo(bookDto);
    }

    @Test
    public void update_bookDtoWithNonExistingId_illegalStateExceptionThrown() {
        var bookDto = new BookDto.Builder().build();

        when(bookServiceImpl.update(eq(bookDto))).thenThrow(new IllegalStateException());

        Assertions.assertThatThrownBy(() -> bookController.update(bookDto)).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void update_bookDtoWithExistingId_bookDtoIsUpdated() {
        BookDto bookDto = new BookDto.Builder(1L)
                .name("Harry Potter")
                .author("J. K. Rowling")
                .isbn("4M524J6")
                .build();

        when(bookServiceImpl.update(eq(bookDto))).thenReturn(bookDto);

        var result = bookController.update(bookDto).getBody();

        verify(bookServiceImpl).update(bookDto);
        assertThat(result).isEqualTo(bookDto);
    }
}
